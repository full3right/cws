import Ember from 'ember';
import config from '../config/environment';


export function workerEarnperday(hashrates) {
  var Earnperday = 24 * 60 * 60 / config.APP.BlockTime * (hashrates[0] / hashrates[1]) * config.APP.BlockReward;
      if (!Earnperday) {
      return 0;
}
      return Earnperday.toFixed();
}
export default Ember.Helper.helper(workerEarnperday);
