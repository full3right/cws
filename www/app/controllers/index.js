import Ember from 'ember';

export default Ember.Controller.extend({
  applicationController: Ember.inject.controller('application'),
  stats: Ember.computed.reads('applicationController'),
  config: Ember.computed.reads('applicationController.config'),
  settings: Ember.computed.reads('applicationController.model.settings'),

  // try to read some settings from the model.settings
  PayoutThreshold: Ember.computed('settings', {
    get() {
      var threshold = this.get('settings.PayoutThreshold');
      if (threshold) {
        // in shannon (10**9)
        return threshold / 1000000000;
      }
      return this.get('config').PayoutThreshold;
    }
  }),

  PayoutInterval: Ember.computed('settings', {
    get() {
      var interval = this.get('settings.PayoutInterval');
      if (interval) {
        return interval;
      }
      return this.get('config').PayoutInterval;
    }
  }),

  PoolFee: Ember.computed('settings', {
    get() {
      var poolfee = this.get('settings.PoolFee');
      if (poolfee) {
        return poolfee + '%';
      }
      return this.get('config').PoolFee;
    }
  }),

	cachedLogin: Ember.computed('login', {
    get() {
      return this.get('login') || Ember.$.cookie('login');
    },
    set(key, value) {
      Ember.$.cookie('login', value);
      this.set('model.login', value);
      return value;
    }
  }),
  chartOptions: Ember.computed("model.hashrate", {
        get() {
            var e = this,
                t = e.getWithDefault("stats.model.poolCharts"),
                a = {
                    chart: {
                        backgroundColor: "rgba(255, 255, 255, 0.1)",
                        type: "areaspline",
                        styledMode: true,
                        height: 300,
                        marginRight: 10,
                        events: {
                            load: function() {
                                var series = this.series[0];
                                setInterval(function() {
                                    var x = (new Date()).getTime(), y = e.getWithDefault("model.Hashrate") / 1000000;
                                    series.addPoint([x, y], true, true);
                                }, 1200000);
                            }
                        }
                    },
                    title: {
                        text: "Our pool's hashrate",
                        style: {
                            color: "#dedede"
                        }
                    },
        plotOptions: {
            areaspline: {
                fillOpacity: 0.05,
                marker:{ 
                  radius: 0,
                },
            },
        },                 
    xAxis: {
               visible: false,
          labels: { enbled: false, },

    },
    yAxis: {
          visible: false,
          labels: { enbled: false, },

    },

       plotLines: [{
         value: 0,
           width: 1,
             color: "#dedede"
                    }],
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        formatter: function() {
                            return this.y > 1000000000000 ? "<b>" + this.point.d + "<b><br>Hashrate&nbsp;" + (this.y / 1000000000000).toFixed(2) + "&nbsp;TH/s</b>" : this.y > 1000000000 ? "<b>" + this.point.d + "<b><br>Hashrate&nbsp;" + (this.y / 1000000000).toFixed(2) + "&nbsp;GH/s</b>" : this.y > 1000000 ? "<b>" + this.point.d + "<b><br>Hashrate&nbsp;" + (this.y / 1000000).toFixed(2) + "&nbsp;MH/s</b>" : "<b>" + this.point.d + "<b><br>Hashrate<b>&nbsp;" + this.y.toFixed(2) + "&nbsp;H/s</b>";
                        },
                        useHTML: true
                    },
                    exporting: {
                        enabled: false
                    },
                    series: [{
                        color: "#cc9900",
                        name: "Hashrate",
                        data: function() {
                            var e, a = [];
                            if (null != t) {
                                for (e = 0; e <= t.length - 1; e += 1) {
                                    var n = 0,
                                        r = 0,
                                        l = 0;
                                    r = new Date(1e3 * t[e].x);
                                    l = r.toLocaleString();
                                    n = t[e].y; a.push({
                                        x: r,
                                        d: l,
                                        y: n
                                    });
                                }
                            } else {
                                a.push({
                                x: 0,
                                d: 0,
                                y: 0
                                });
                            }
                            return a;
                        }()
                    }]
                };
            return a;
        }
    })
});
//var l10nEN = new Intl.NumberFormat("en-US");
//var l10nUSD = new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", minimumFractionDigits: "4"});
//var l10nGBP = new Intl.NumberFormat("en-GB", { style: "currency", currency: "GBP" });
//var l10nEUR = new Intl.NumberFormat("de-DE", { style: "currency", currency: "EUR", minimumFractionDigits: "2" });

/*var hashes = function(a) {
  if (a > 999999999) {
    return "" + Math.round(a / 1000000) / 1000 + " GH";
  }
  if (a > 999999) {
    return "" + Math.round(a / 1000) / 1000 + " MH";
  }
  if (a > 999) {
    return "" + Math.round(a) / 1000 + " KH";
  }
  return "" + a + " H";
};*/
function getPriceInfo() {
  var xhr = new XMLHttpRequest();
  var url = 'https://min-api.cryptocompare.com/data/price?fsym=CLO&tsyms=USD';
  
  xhr.onreadystatechange = function () {
    if (this.readyState === 4 && this.status === 200) {
      var coinResponse2 = JSON.parse(this.responseText);
      document.getElementById("average-usd").innerHTML = coinResponse2.USD;
    }
  };
  
  xhr.open("GET", url, true);
  xhr.send();
  
  try {
    // Compliant browsers
    xhr = new XMLHttpRequest();
  }

  catch (e) {
    try {
      // IE7+
      //xhr = new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch (e) {
      // AJAX is not supported
      alert('AJAX is not supported. Please upgrade your browser!');
    }
  }
}

getPriceInfo();
